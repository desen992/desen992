package ru.mephi;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class IpReducer extends Reducer<Text, IntWritable, Text, Text> {
    private Text result = new Text();

    public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

        double avg;
        int total = 0;
        double i = 0;

        for (IntWritable bytes : values) {
            total += Integer.parseInt(bytes.toString());
            i++;
        }

        avg = total / i;

        result.set(avg + "," + total);

        context.write(key, result);
    }
}
