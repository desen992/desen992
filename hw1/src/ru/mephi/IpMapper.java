package ru.mephi;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IpMapper extends Mapper<Object, Text, Text, IntWritable> {

    private Text ip = new Text();
    private IntWritable bytes = new IntWritable();
    public final static String ip_regex = "^([\\d.]+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(.+?)\" (\\d{3}) (\\d+) \"([^\"]+)\" \"([^\"]+)\" \"([^\"]+)\"";


    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String record = value.toString();

        Pattern pattern = Pattern.compile(ip_regex);
        Matcher matcher = pattern.matcher(record);

        if (!matcher.matches()) return;

        ip.set(matcher.group(1));
        bytes.set(Integer.parseInt(matcher.group(7)));

        context.write(ip, bytes);
    }
}
