package ru.mephi;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileAsBinaryOutputFormat;

import java.util.regex.Pattern;

public class Main extends Configured implements Tool {

    public final static String ip_regex = "^([\\d.]+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(.+?)\" (\\d{3}) (\\d+) \"([^\"]+)\" \"([^\"]+)\" \"([^\"]+)\"";

    //public static Pattern pattern;

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        int res = ToolRunner.run(conf, new Main(), args);
        System.exit(res);
    }

    public int run(String[] args) throws Exception {


        Configuration conf = new Configuration();

        //enable compression
        conf.setBoolean("mapreduce.output.fileoutputformat.compress", true);
        conf.set("mapreduce.output.fileoutputformat.compress.type", "BLOCK");
        conf.setClass("mapreduce.output.fileoutputformat.compress.codec", org.apache.hadoop.io.compress.SnappyCodec.class, org.apache.hadoop.io.compress.CompressionCodec.class);
//*/


        Job job = Job.getInstance(conf, "IpMR Job");

        //setting classes into configuration
        job.setJarByClass(Main.class);

        job.setMapperClass(IpMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setCombinerClass(IpCombiner.class);

        job.setReducerClass(IpReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        //Input
        FileInputFormat.setInputPaths(job, args[0]);

        //Output
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setOutputFormatClass(SequenceFileAsBinaryOutputFormat.class);

        return job.waitForCompletion(true) ? 0 : 1;
    }
}
