package ru.mephi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;


public class MainTest {

    MapDriver<Object, Text, Text, IntWritable> mapperDriver;
    ReduceDriver<Text, IntWritable, Text, Text> reduceDriver;
    ReduceDriver<Text, IntWritable, Text, IntWritable> combinerDriver;

    @Before
    public void setUp() {
        IpMapper mapper = new IpMapper();
        IpReducer reducer = new IpReducer();
        IpCombiner combiner = new IpCombiner();
        mapperDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        combinerDriver = ReduceDriver.newReduceDriver(combiner);
        //Main.pattern = Pattern.compile(Main.ip_regex);
    }

    @Test
    public void MapperTest() throws IOException {
        mapperDriver.withInput(new LongWritable(), new Text("46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] \"GET /administrator/ HTTP/1.1\" 200 4263 \"-\" \"Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0\" \"-\""));
        mapperDriver.withOutput(new Text("46.72.177.4"), new IntWritable(4263));

        mapperDriver.runTest();
    }

    @Test
    public void CombinerTest() throws IOException {
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(100));
        values.add(new IntWritable(200));
        combinerDriver.withInput(new Text("109.169.248.247"), values);
        combinerDriver.withOutput(new Text("109.169.248.247"), new IntWritable(300));

        combinerDriver.runTest();
    }


    @Test
    public void ReducerTest() throws IOException {
        List<IntWritable> values1 = new ArrayList<IntWritable>();
        values1.add(new IntWritable(1220));
        values1.add(new IntWritable(2346));
        values1.add(new IntWritable(1234));
        values1.add(new IntWritable(5555));
        reduceDriver.withInput(new Text("109.169.248.247"), values1);

        List<IntWritable> values2 = new ArrayList<IntWritable>();
        values2.add(new IntWritable(123));
        values2.add(new IntWritable(234));
        values2.add(new IntWritable(124));
        values2.add(new IntWritable(555));
        reduceDriver.withInput(new Text("95.140.24.131"), values2);

        reduceDriver.withOutput(new Text("109.169.248.247"), new Text("2588.75,10355"));
        reduceDriver.withOutput(new Text("95.140.24.131"), new Text("259.0,1036"));

        reduceDriver.runTest();
    }
}