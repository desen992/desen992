
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;
import scala.Tuple3;

import java.io.IOException;

public class MetricsAgregator {

    public static void main(String[] args) {

        //Create context and dataset
        JavaSparkContext sc = new JavaSparkContext("local[2]", "Metrics",
                "$/usr/local/spark", new String[]{"/jars/hw2.jar"});
        JavaRDD<String> dataset = sc
                .textFile("hdfs:///hdata/input/RawData.txt");

        //    work with dataset
            //time stamp
        JavaPairRDD<Integer, Long> stamps = dataset.mapToPair(s -> new Tuple2<>(
                Integer.parseInt(s.split(",")[0]),
                Long.parseLong(s.split(",")[1])
        ));

        //value
        JavaPairRDD<Integer, Double> metrics = dataset.mapToPair(
                s -> new Tuple2<>(
                        Integer.parseInt(s.split(",")[0]),
                        Double.parseDouble(s.split(",")[2])
                )
        );
 
        //avg value
        Function<Double, Counter> createComb = (Double y) -> new Counter(y, 1);
        Function2<Counter, Double, Counter> addAndCount = (Counter x, Double y) -> {
            x.addTotal(y);
            x.addCount(1);
            return x;
        };
        Function2<Counter, Counter, Counter> combine = (Counter x, Counter y) -> {
            x.addTotal(y.getTotal());
            x.addCount(y.getCount());
            return x;
        };

        JavaPairRDD<Integer, Double> avg = metrics.combineByKey(createComb, addAndCount, combine)
                .mapToPair((PairFunction<Tuple2<Integer, Counter>, Integer, Double>) f -> new Tuple2<>(f._1, f._2.avg()));

        //max value of time in scale per id
        JavaPairRDD<Integer, Long> max = stamps.reduceByKey((v1, v2) -> Math.max(v1, v2));

        //join datasets
        JavaPairRDD<Integer, Tuple3<Long, String, Double>> joined = max.join(avg)
                .mapValues((Function<Tuple2<Long, Double>, Tuple3<Long, String, Double>>) f -> new Tuple3<>(f._1, args[1], f._2)).sortByKey();

        //Save to output directory
        joined.saveAsTextFile(args[0]);
    }
}
