import java.io.Serializable;

public class Counter implements Serializable {

    private Double total;
    private Integer count;

    public Counter(Double total_value, Integer count_value) {
        total = total_value;
        count = count_value;
    }

    public Double getTotal(){
        return total;
    }

    public Integer getCount(){
        return count;
    }
    public Double avg() {
        return total / count;
    }

    public void addTotal(Double v) {
        total += v;
    }
    public void addCount(Integer v) {
        count += v;
    }
}
