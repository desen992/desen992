import com.holdenkarau.spark.testing.JavaRDDComparisons;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.*;
import scala.Tuple2;
import scala.Tuple3;

import com.holdenkarau.spark.testing.SharedJavaSparkContext;
import org.junit.Before;
import org.junit.Test;
import scala.reflect.ClassTag;

public class MainTest extends SharedJavaSparkContext implements Serializable {

    private static List<String> metrics;
    private static List<Tuple2<Integer, Long>> validation_value1;
    private static List<Tuple2<Integer, Double>> validation_value2;
    private static List<Tuple2<Integer, Double>> validation_value3;
    private static List<Tuple2<Integer, Long>> validation_value4;
    private static List<Tuple2<Integer, Tuple3<Long, String, Double>>> validation_value5;

    @Before
    public void setUp() {
        metrics = Arrays.asList("2,1397463641,28.0",
                "1,1007601605,16.0",
                "2,1186879717,37.0"
        );
        validation_value1 = Arrays.asList(new Tuple2<>(2, Long.valueOf(1397463641)),
                new Tuple2<>(1, Long.valueOf(1007601605)), new Tuple2<>(2, Long.valueOf(1186879717)));
        validation_value2 = Arrays.asList(new Tuple2<>(2, 28.0), new Tuple2<>(1, 16.0), new Tuple2<>(2, 37.0));
        validation_value3 = Arrays.asList(new Tuple2<>(1, 16.0), new Tuple2<>(2, 32.5));
        validation_value4 = Arrays.asList(new Tuple2<>(1, Long.valueOf(1007601605)), new Tuple2<>(2, Long.valueOf(1186879717)));
        validation_value5 = Arrays.asList(new Tuple2<>(1,new Tuple3<>(Long.valueOf(1007601605),"1m", 16.0)),
                new Tuple2<>(2,new Tuple3<>(Long.valueOf(1186879717),"1m", 32.5)));
    }

    @Test
    public void split() {
        JavaRDD<String> rdd1 = jsc().parallelize(metrics);
        JavaPairRDD<Integer, Double> valid_value = jsc().parallelizePairs(validation_value2);

        JavaPairRDD<Integer, Double> result = rdd1.mapToPair(
                s -> new Tuple2<>(
                        Integer.parseInt(s.split(",")[0]),
                        Double.parseDouble(s.split(",")[2])
                )
        );

        ClassTag<Tuple2<Integer, Double>> tag = scala.reflect.ClassTag$.MODULE$.apply(Tuple2.class);

        JavaRDDComparisons.assertRDDEquals(
                JavaRDD.fromRDD(JavaPairRDD.toRDD(result), tag),
                JavaRDD.fromRDD(JavaPairRDD.toRDD(valid_value), tag));
    }

    @Test
    public void count() {
        JavaPairRDD<Integer, Double> rdd1 = jsc().parallelizePairs(validation_value2);
        JavaPairRDD<Integer, Double> valid_value = jsc().parallelizePairs(validation_value3);

        Function<Double, Counter> createComb = (Double y) -> new Counter(y, 1);
        Function2<Counter, Double, Counter> addAndCount = (Counter x, Double y) -> {
            x.addTotal(y);
            x.addCount(1);
            return x;
        };
        Function2<Counter, Counter, Counter> combine = (Counter x, Counter y) -> {
            x.addTotal(y.getTotal());
            x.addCount(y.getCount());
            return x;
        };

        JavaPairRDD<Integer, Double> result = rdd1.combineByKey(createComb, addAndCount, combine)
                .mapToPair((PairFunction<Tuple2<Integer, Counter>, Integer, Double>) f -> new Tuple2<>(f._1, f._2.avg()));

        ClassTag<Tuple2<Integer, Double>> tag = scala.reflect.ClassTag$.MODULE$.apply(Tuple2.class);

        JavaRDDComparisons.assertRDDEquals(
                JavaRDD.fromRDD(JavaPairRDD.toRDD(result), tag),
                JavaRDD.fromRDD(JavaPairRDD.toRDD(valid_value), tag));
    }

    @Test
    public void join() {
        JavaPairRDD<Integer, Long> rdd1 = jsc().parallelizePairs(validation_value4);
        JavaPairRDD<Integer, Double> rdd2 = jsc().parallelizePairs(validation_value3);
        JavaPairRDD<Integer, Tuple3<Long, String, Double>> valid_value = jsc().parallelizePairs(validation_value5);

        JavaPairRDD<Integer, Tuple3<Long, String, Double>> result = rdd1.join(rdd2)
                .mapValues((Function<Tuple2<Long, Double>, Tuple3<Long, String, Double>>) f -> new Tuple3<>(f._1,"1m",f._2));

        ClassTag<Tuple2<Integer, Tuple3<Long, String, Double>>> tag = scala.reflect.ClassTag$.MODULE$.apply(Tuple2.class);

        JavaRDDComparisons.assertRDDEquals(
                JavaRDD.fromRDD(JavaPairRDD.toRDD(result), tag),
                JavaRDD.fromRDD(JavaPairRDD.toRDD(valid_value), tag));
    }

}
