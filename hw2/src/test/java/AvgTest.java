import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.*;
import static org.junit.Assert.assertEquals;

public class AvgTest {

    private static Double total;
    private static Integer count;

    @Before
    public void setUp() {
        total = 17.5;
        count = 10;
    }

    @Test
    public void CounterClassTest() {
        Counter test = new Counter(total, count);
        assertNotNull(test);
        assertEquals(17.5, test.getTotal(), 0.000001);
        assertEquals(Integer.valueOf(10), test.getCount());
        test.addTotal(2.0);
        assertEquals(19.5, test.getTotal(), 0.000001);
        test.addCount(1);
        assertEquals(Integer.valueOf(11), test.getCount());
        assertEquals(19.5 / 11, test.avg(), 0.000001);
    }

}