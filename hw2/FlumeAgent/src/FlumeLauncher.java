import org.apache.flume.node.Application;

public class FlumeLauncher {
    public static void main(String[] args) {
        Application.main(new String[]{
                "-f", "flume.conf", // путь до файла с конфигурацией
                "-n", "hw2agent"                   // имя агента
        });
    }
}

